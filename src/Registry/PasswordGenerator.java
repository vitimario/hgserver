package Registry;

public class PasswordGenerator {
	
	private static int counter = 0;
	private static Object lock = new Object();

	public static int getPassword() {
		synchronized(lock) {
			return counter++;
		}
	}
}
