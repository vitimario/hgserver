# README #

The Client version of this app is available at  
https://vitimario@bitbucket.org/vitimario/hgclient.git

Documentation @  
https://drive.google.com/file/d/0B6wlleL2j4psLXZ4N3Z0dEZDRDQ/view

### What is this repository for? ###

* Networking exam UNIPI RCL
* Version 0.9

### How do I get set up? ###

Eclipse-> import -> Git -> Projects From Git -> clone URI -> specify the URI to clone
this runs under JRE IcedTea 2.5.4